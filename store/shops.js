// ACTION TYPES
const ADD_LIST = "ADD_LIST";
const DELETE_LIST = "DELETE_LIST";
const UPDATE_USER = "UPDATE_USER";
const ADD_ITEM_TO_LIST = "ADD_ITEM_TO_LIST";
const CHANGE_ITEM_BUY = "CHANGE_ITEM_BUY";
const RESET_BUY_ITEM = "RESET_BUY_ITEM";
const DELETE_ITEM = "DELETE_ITEM";
const UPDATE_ITEM = "UPDATE_ITEM";
//SELECTORS
const MODULE_NAME = "shops";
export const getShops = (state) => state[MODULE_NAME].shopList;
export const getUser = (state) => state[MODULE_NAME].user;
export const getAllData =(state)=>state[MODULE_NAME];
//REDUCER
const initialState = {
  user: {
    username: "aykhan_hajiyev",
    image: "",
  },
  shopList: [],
};

export function shopListReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_LIST:
      return {
        ...state,
        shopList: [
          {
            id: `${Math.random()}${Date.now()}`,
            name: payload.name,
            type: payload.type,
            items: [],
          },
          ...state.shopList,
        ],
      };
    case DELETE_LIST:
      return {
        ...state,
        shopList: state.shopList.filter(
          (shopItem) => shopItem.id !== payload.shopItemID
        ),
      };
    case UPDATE_USER:
      return {
        ...state,
        user: {
          ...state.user,
          username: payload.username,
          image: payload.image,
        },
      };
    case ADD_ITEM_TO_LIST:
      return {
        ...state,
        shopList: state.shopList.map((shopItem) => {
          if (shopItem.id === payload.shopItemID) {
            return {
              ...shopItem,
              items: [
                {
                  id: `${Math.random()}${Date.now()}`,
                  name: payload.itemName,
                  count: payload.itemCount,
                  weight: payload.itemWeight,
                  isBought: false,
                },
                ...shopItem.items,
              ],
            };
          }
          return shopItem;
        }),
      };
    case CHANGE_ITEM_BUY:
      return {
        ...state,
        shopList: state.shopList.map((shopItem) => {
          if (shopItem.id === payload.shopItemID) {
            return {
              ...shopItem,
              items: shopItem.items.map((item) => {
                if (item.id === payload.itemID) {
                  return {
                    ...item,
                    isBought: !item.isBought,
                  };
                }
                return item;
              }),
            };
          }
          return shopItem;
        }),
      };
    case RESET_BUY_ITEM:
      return {
        ...state,
        shopList: state.shopList.map((shopItem) => {
          if (shopItem.id === payload.shopItemID) {
            return {
              ...shopItem,
              items: shopItem.items.map((item) => {
                return {
                  ...item,
                  isBought: false,
                };
              }),
            };
          }
          return shopItem;
        }),
      };
    case DELETE_ITEM:
      return {
        ...state,
        shopList: state.shopList.map((shopItem) => {
          if (shopItem.id === payload.shopItemID) {
            return {
              ...shopItem,
              items: shopItem.items.filter(
                (item) => item.id !== payload.itemID
              ),
            };
          }
          return shopItem;
        }),
      };
    case UPDATE_ITEM:
      return {
        ...state,
        shopList: state.shopList.map((shopItem) => {
          if (shopItem.id === payload.shopItemID) {
            return {
              ...shopItem,
              items: shopItem.items.map((item) => {
                if (item.id === payload.item.id) {
                  return {
                    ...item,
                    ...payload.item,
                  };
                }
                return item;
              }),
            };
          }
          return shopItem;
        }),
      };
    default:
      return state;
  }
}
//ACTION CREATORS
export const addList = (payload) => ({ type: ADD_LIST, payload });
export const deleteList = (payload) => ({ type: DELETE_LIST, payload });
export const updateUser = (payload) => ({ type: UPDATE_USER, payload });
export const addItemToList = (payload) => ({ type: ADD_ITEM_TO_LIST, payload });
export const changeItemBuy = (payload) => ({ type: CHANGE_ITEM_BUY, payload });
export const resetBuyItem = (payload) => ({ type: RESET_BUY_ITEM, payload });
export const deleteItem = (payload) => ({ type: DELETE_ITEM, payload });
export const updateItem = (payload) => ({ type: UPDATE_ITEM, payload });
// {
//   id: "1",
//   name: "Everything for breakfast",
//   type: "Regular",
//   items: [
//     {
//       id: "1",
//       name: "milk",
//       count: 2,
//       weightType: "litre",
//       isBought:false
//     },
//   ],
// },
// {
//   id: "2",
//   name: "Evening with Pasta",
//   type: "Regular",
//   items: [
//     {
//       id: "1",
//       name: "Salt",
//       count: 1,
//       weightType: "pkg",
//     },
//     {
//       id: "2",
//       name: "Pasta",
//       count: 2,
//       weightType: "pkg",
//     },
//   ],
// },
// {
//   id: "3",
//   name: "Kitchen repair",
//   type: "One Time",
//   items: [
//     {
//       id: "1",
//       name: "Salt",
//       count: 1,
//       weightType: "pkg",
//     },
//     {
//       id: "2",
//       name: "Pasta",
//       count: 2,
//       weightType: "pkg",
//     },
//   ],
// },
