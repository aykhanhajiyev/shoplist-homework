import { createStore, combineReducers } from "redux";
import { shopListReducer } from "./shops";

const rootReducer = combineReducers({
  shops: shopListReducer,
});

const store = createStore(rootReducer);

export default store;
