import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { OneTimeStack } from "./OneTimeStack";
import { RegularStack } from "./RegularStack";
import { AddListStack } from "./AddListStack";
import { UserSettingsStack } from "./UserSettingsStack";

import { CustomDrawer } from "../commons/CustomDrawer";
import { connect } from "react-redux";
import { getUser } from "../store/shops";

const { Navigator, Screen } = createDrawerNavigator();

const mapStateToProps = (state) => ({
  user: getUser(state),
});

export const RootNav = connect(mapStateToProps)((props) => {
  const {username,image} = props.user;
  return (
    <NavigationContainer>
      <Navigator drawerContent={(props)=><CustomDrawer {...props} username={username} imageUrl={image}/>}>
        <Screen name="OneTime" component={OneTimeStack} />
        <Screen name="Regular" component={RegularStack} />
        <Screen name="AddList" component={AddListStack} />
        <Screen name="UserSettings" component={UserSettingsStack} />
      </Navigator>
    </NavigationContainer>
  );
});
