import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { AddListScreen } from "../screens";
import headerStyleOptions from "../styles/headerStyleOptions";
const { Navigator, Screen } = createStackNavigator();
export const AddListStack = ({ navigation }) => {
  return (
    <Navigator>
      <Screen
        name="AddListScreen"
        component={AddListScreen}
        options={{
          title: "New List",
          ...headerStyleOptions,
        }}
      />
    </Navigator>
  );
};
