import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { OneTimeListsScreen, SingleScreen, SingleEditScreen } from "../screens";
import headerStyleOptions from "../styles/headerStyleOptions";
import headerSinglePageOptions from "../styles/headerSinglePageOptions";
import { MenuIcon } from "../commons/MenuIcon";
const { Navigator, Screen } = createStackNavigator();
export const OneTimeStack = ({navigation}) => {
  return (
    <Navigator>
      <Screen
        name="OneTimeLists"
        component={OneTimeListsScreen}
        options={{
          title: "One Time Lists",
          ...headerStyleOptions,
          headerRight:()=><MenuIcon onPress={navigation.toggleDrawer}/>

        }}
      />
      <Screen
        name="SingleScreen"
        component={SingleScreen}
        options={{
          title: "TITLE ONE TIME LIST",
          ...headerStyleOptions,
          ...headerSinglePageOptions,
        }}
      />
      <Screen
        name="SingleEditScreen"
        component={SingleEditScreen}
        options={{
          title: "Title ",
          ...headerStyleOptions,
          ...headerSinglePageOptions,
        }}
      />
    </Navigator>
  );
};
