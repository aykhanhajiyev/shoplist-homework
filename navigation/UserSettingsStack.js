import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { UserSettingsScreen } from "../screens";
import headerStyleOptions from "../styles/headerStyleOptions";
const { Navigator, Screen } = createStackNavigator();
export const UserSettingsStack = ({ navigation }) => {
  return (
    <Navigator>
      <Screen
        name="UserSettingsScreen"
        component={UserSettingsScreen}
        options={{
          title: "User Settings",
          ...headerStyleOptions,
        }}
      />
    </Navigator>
  );
};
