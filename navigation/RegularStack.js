import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { RegularListsScreen, SingleScreen } from "../screens";

import headerStyleOptions from "../styles/headerStyleOptions";
import headerSinglePageOptions from "../styles/headerSinglePageOptions";

import { MenuIcon } from "../commons/MenuIcon";
import { SingleEditScreen } from "../screens/SingleEditScreen";

const { Navigator, Screen } = createStackNavigator();
export const RegularStack = ({ navigation}) => {
  return (
    <Navigator>
      <Screen
        name="RegularLists"
        component={RegularListsScreen}
        options={{
          title: "Regular Lists",
          ...headerStyleOptions,
          headerRight: () => <MenuIcon onPress={navigation.toggleDrawer} />,
        }}
      />
      <Screen
        name="SingleScreen"
        component={SingleScreen}
        options={{
          ...headerStyleOptions,
          ...headerSinglePageOptions,
        }}
      />
      <Screen
        name="SingleEditScreen"
        component={SingleEditScreen}
        options={{
          title: "Title ",
          ...headerStyleOptions,
          ...headerSinglePageOptions,
        }}
      />
    </Navigator>
  );
};
