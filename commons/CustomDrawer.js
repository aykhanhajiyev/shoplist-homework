import React from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";

import COLORS from "../styles/colors";
import { CustomText } from "../components";

export const CustomDrawer = (props) => {
  const {username,imageUrl} =props;
  return (
    <View style={styles.container}>
      <View style={styles.headerRow}>
        <Image
          source={{
            uri:
              (imageUrl!=='' ) ? imageUrl : "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png",
          }}
          style={styles.headerImg}
        />
        <CustomText style={styles.headerText}>{username}</CustomText>
      </View>
      <View style={styles.listWrapper}>
        <TouchableOpacity style={styles.listItem}>
          <CustomText
            weight="semibold"
            style={styles.listItemText}
            onPress={() => props.navigation.navigate("AddList")}
          >
            Add New List
          </CustomText>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.listItem, { marginVertical: 10 }]}
          onPress={() => props.navigation.navigate("OneTime")}
        >
          <CustomText weight="semibold" style={styles.listItemText}>
            One Time List
          </CustomText>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.listItem, { marginVertical: 10 }]}
          onPress={() => props.navigation.navigate("Regular")}
        >
          <CustomText weight="semibold" style={styles.listItemText}>
            Regular Lists
          </CustomText>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.listItem, { marginVertical: 10 }]}
          onPress={() => props.navigation.navigate("UserSettings")}
        >
          <CustomText weight="semibold" style={styles.listItemText}>
            User Settings
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    paddingTop: 20,
  },
  headerRow: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  headerImg: {
    width: 70,
    height: 70,
    borderColor: COLORS.main,
    borderRadius: 35,
    borderWidth: 1,
  },
  headerText: {
    fontSize: 20,
    paddingTop: 20,
    paddingLeft: 10,
  },
  listWrapper: {
    backgroundColor: COLORS.main,
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    height: "100%",
  },
  listItem: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 20,
    marginHorizontal: 20,
    marginVertical: 30,
  },
  listItemText: {
    color: COLORS.main,
    textAlign: "center",
    textTransform: "uppercase",
  },
});
