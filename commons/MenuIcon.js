import React from "react";
import { TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
export const MenuIcon = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Ionicons name="ios-menu" size={34} color="white" />
    </TouchableOpacity>
  );
};
