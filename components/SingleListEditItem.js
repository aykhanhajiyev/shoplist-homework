import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import { CustomText } from "./CustomText";

import { FontAwesome5 } from "@expo/vector-icons";
import COLORS from "../styles/colors";

export const SingleListEditItem = ({itemName,itemCount, itemWeight, deleteItemHandler, editItemHandler}) => {
  return (
    <TouchableOpacity style={styles.itemRow}>
      <View style={styles.itemLeft}>
        <TouchableOpacity style={styles.editIconWrapper} onPress={editItemHandler}>
          <FontAwesome5 name="pen" size={24} color="white" />
        </TouchableOpacity>
        <CustomText style={styles.itemName}>{itemName}</CustomText>
      </View>
      <View style={styles.itemRight}>
        <CustomText style={styles.itemCount}>x{itemCount} {itemWeight} </CustomText>
        <TouchableOpacity style={styles.deleteIconWrapper} onPress={deleteItemHandler}>
          <CustomText style={{ color: "white", fontSize: 28 }}>X</CustomText>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  itemRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: COLORS.secondary,
    borderRadius: 20,
    marginBottom:15
  },
  itemLeft: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  itemRight: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  itemName: {
    paddingLeft: 10,
    fontSize: 16,
  },
  itemCount: {
    fontSize: 16,
  },
  editIconWrapper: {
    backgroundColor: COLORS.secondary,
    width: 40,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
  },
  deleteIconWrapper: {
    backgroundColor: COLORS.main,
    width: 40,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    overflow: "hidden",
  },
});
