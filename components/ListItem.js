import React from "react";
import { TouchableOpacity, View, StyleSheet } from "react-native";
import { CustomText } from "./CustomText";
import COLORS from "../styles/colors";
export const ListItem = ({ item, isRegular, onPress, onLongPress }) => {
  const boughtItemsCount = item.items.filter((i) => i.isBought === true).length;
  const totalItemsCount = item.items.length;
  const percentage = (boughtItemsCount / totalItemsCount) * 100;
  const isCompleted = percentage === 100 && !isRegular;
  return (
    <TouchableOpacity style={styles.cardItem} onPress={onPress} onLongPress={onLongPress}>
      <View style={styles.textWrapper} opacity={isCompleted ? 0.4 : 1}>
        <CustomText weight="bold" style={styles.cardText}>
          {item.name}
        </CustomText>
        <CustomText weight="semibold">
          {boughtItemsCount} / {totalItemsCount}
        </CustomText>
      </View>
      <View style={styles.loaderWrapper} opacity={isCompleted ? 0.3 : 1}>
        <View
          style={[
            { width: totalItemsCount !== 0 ? percentage + "%" : 0 },
            styles.loader,
          ]}
        ></View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 10,
  },
  cardItem: {
    marginBottom: 20,
    borderWidth: 1,
    borderColor: COLORS.light,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 10,
    borderRadius: 10,
  },
  cardText: {
    fontSize: 16,
  },
  loaderWrapper: {
    backgroundColor: COLORS.silver,
    height: 30,
    width: "100%",
    borderRadius: 20,
    marginTop: 10,
  },
  loader: {
    backgroundColor: COLORS.secondary,
    height: 30,
    borderRadius: 20,
  },
});
