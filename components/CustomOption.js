import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import { CustomText } from "./CustomText";
import COLORS from "../styles/colors";

export const CustomOption = ({ options,selected,onChange }) => {
    const isSelected = options.some(option=>option===selected);
  return (
    <View style={styles.row}>
      {options.map((option) => (
        <TouchableOpacity
          style={[
            styles.optionWrapper,
            { width: 100 / options.length - 5 + "%", backgroundColor:(isSelected && option===selected) ? "rgba(238,238,238,0.5)" : COLORS.silver},
          ]}
          key={option}
          onPress={()=>onChange(option)}
        >
          <CustomText
            style={[styles.optionText, {opacity:(isSelected && option===selected) ? 1 : 0.4}]}
            weight={isSelected && option===selected ? "bold" : "regular"}
          >
            {option}
          </CustomText>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    paddingHorizontal: 20,
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 20,
  },
  optionWrapper: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 20,
    marginHorizontal: 10,
  },
  optionText: {
    textAlign: "center",
  },
});
