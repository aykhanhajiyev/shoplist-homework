import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import { CustomText } from "./CustomText";

import COLORS from "../styles/colors";

export const SingleListItem = ({
  itemName,
  itemCount,
  itemWeight,
  itemIsBought,
  onLongPress,
}) => {
  return (
    <TouchableOpacity
    activeOpacity={0.6}
      style={styles.itemRow}
      onLongPress={onLongPress}>
      <View style={styles.itemLeft} opacity={(itemIsBought) ? 0.4 : 1}>
        <CustomText style={styles.itemName}>{itemName}</CustomText>
      </View>
      <View style={styles.itemRight} opacity={(itemIsBought) ? 0.4 : 1}>
        <CustomText style={styles.itemCount}>
          x{itemCount} {itemWeight}{" "}
        </CustomText>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  itemRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: COLORS.secondary,
    borderRadius: 20,
    marginBottom: 15,
    padding: 15,
  },
  itemLeft: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  itemRight: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  itemName: {
    paddingLeft: 10,
    fontSize: 16,
  },
  itemCount: {
    fontSize: 16,
  },
});
