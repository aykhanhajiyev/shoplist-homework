import React from "react";
import { TextInput, StyleSheet,View } from "react-native";
import COLORS from "../styles/colors";

export const CustomInput = ({style,...rest}) => {
  return (
    <View style={styles.fieldWrapper}>
      <TextInput style={[styles.field,style]} {...rest}/>
    </View>
  );
};

const styles = StyleSheet.create({
  fieldWrapper: {
      width:"90%",
      paddingHorizontal:10
  },
  field: {
      backgroundColor:COLORS.silver,
      height:40,
      paddingHorizontal:5,
      textAlign:'center',
      borderRadius:20,
      fontFamily:'MontserratRegular',
      fontSize:18,
  },
});
