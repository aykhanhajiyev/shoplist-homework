import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
  View,
} from "react-native";
import { CustomText } from "./CustomText";
import COLORS from "../styles/colors";

export const CustomButton = ({ title, onPress,style,innerTextStyle,innerBtnTextWrapperStyle, ...rest }) => {
  const Touchable =
    Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
  return (
    <View style={[styles.container, style]}>
      <Touchable onPress={onPress} {...rest}>
        <View style={[styles.btnText,{...innerBtnTextWrapperStyle}]}>
          <CustomText weight="bold" style={[styles.title,{...innerTextStyle}]}>{title}</CustomText>
        </View>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "90%",
    justifyContent: "center",
    overflow:'hidden',
    borderRadius:50,
    marginTop:20
  },
  btnText: {
    width: "100%",
    backgroundColor: COLORS.main,
    padding: 15,
    borderRadius: 50,
    alignItems: "center",
  },
  title: {
    color: "white",
    textAlign: "center",
    textTransform:'uppercase'
  },
});
