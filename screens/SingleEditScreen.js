import React, { useState,useEffect } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  FlatList,
  Alert,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";

import {
  CustomText,
  CustomInput,
  CustomButton,
  CustomOption,
  SingleListEditItem,
} from "../components";
import COLORS from "../styles/colors";
import { addItemToList, deleteItem, updateItem, getAllData } from "../store/shops";
const mapStateToProps = (state) => ({
  shopList: state.shops.shopList,
  allData : getAllData(state)
});
export const SingleEditScreen = connect(mapStateToProps, {
  addItemToList,
  deleteItem,
  updateItem,
})((props) => {
  const { navigation, addItemToList, shopList, deleteItem, updateItem,allData } = props;
  const { shopItem } = props.route?.params;
  const { items } = shopList.find((item) => item.id === shopItem.id);
  navigation.setOptions({
    title: shopItem.name,
    headerRight: () => (
      <TouchableOpacity onPress={() => navigation.navigate("SingleScreen")}>
        <Ionicons name="ios-save" size={32} color="white" />
      </TouchableOpacity>
    ),
  });

  useEffect(() => {
    AsyncStorage.setItem("shops", JSON.stringify(allData));
    // console.log("SINGLE EDIT SCREEN");
  },[]);

  const [item_Id, setItemId] = useState("");
  const [itemName, setItemName] = useState("");
  const [weight, setWeight] = useState("pkg");
  const [count, setCount] = useState(1);

  const [isEditMode, setIsEditMode] = useState(false);

  const onChange = (weight) => {
    setWeight(weight);
  };
  const decreaseCounter = () => {
    if (count > 1) {
      setCount((count) => count - 1);
    }
  };
  const increaseCounter = () => {
    setCount((count) => count + 1);
  };

  const resetField = () => {
    setItemName("");
    setCount(1);
    setWeight("pkg");
    setIsEditMode(false);
  };

  const addItemtoListHandler = () => {
    if (itemName.trim() !== "") {
      addItemToList({
        shopItemID: shopItem.id,
        itemName,
        itemCount: count,
        itemWeight: weight,
      });
      resetField();
    } else {
      Alert.alert("Please fill list name");
    }
  };

  const deleteItemFromListHandler = (itemId) => {
    Alert.alert("Deleting item", "Are you sure to delete it?", [
      {
        text: "No",
        style: "destructive",
      },
      {
        text: "Yes",
        onPress: () => deleteItem({ shopItemID: shopItem.id, itemID: itemId }),
      },
    ]);
  };
  const editItemFromListHandler = (item) => {
    setCount(item.count);
    setWeight(item.weight);
    setItemName(item.name);
    setIsEditMode(true);
    setItemId(item.id);
  };

  const updateItemAction = () => {
    updateItem({
      shopItemID: shopItem.id,
      item: { id: item_Id, name: itemName, count: count, weight: weight },
    });
  };
  return (
    <TouchableWithoutFeedback style={styles.wrapper} onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <View style={[styles.row, { width: Dimensions.get("window").width }]}>
          <View style={styles.rowWrapper}>
            <CustomText style={{ marginVertical: 10 }}>list name</CustomText>
            <CustomInput
              style={styles.field}
              onChangeText={setItemName}
              value={itemName}
            />
          </View>
          <View style={styles.rowWrapperCount}>
            <CustomText style={{ marginVertical: 10 }}>count</CustomText>
            <View style={styles.rowCounter}>
              <View style={styles.rowMinus}>
                <TouchableOpacity onPress={decreaseCounter}>
                  <CustomText weight="bold" style={styles.iconText}>
                    -
                  </CustomText>
                </TouchableOpacity>
              </View>
              <CustomInput
                style={styles.field}
                value={count.toString()}
                keyboardType="numeric"
                onChangeText={setCount}
              />
              <View style={styles.rowPlus}>
                <TouchableOpacity onPress={increaseCounter}>
                  <CustomText weight="bold" style={styles.iconText}>
                    +
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <CustomOption
          options={["pkg", "kg", "litre", "bott"]}
          selected={weight}
          onChange={onChange}
        />
        <View style={styles.buttonWrapper}>
          {isEditMode ? (
            <View style={styles.buttonEditRow}>
              <CustomButton
                title="Cancel"
                style={styles.cancelBtn}
                onPress={resetField}
              />
              <CustomButton
                title="Update"
                style={styles.updateBtn}
                onPress={updateItemAction}
              />
            </View>
          ) : (
            <CustomButton title="Add To list" onPress={addItemtoListHandler} />
          )}
        </View>
        <View style={styles.horizantalLine} />
        <FlatList
          contentContainerStyle={styles.itemWrapper}
          alwaysBounceVertical={true}
          data={items}
          renderItem={({ item }) => (
            <SingleListEditItem
              itemName={item.name}
              itemCount={item.count}
              itemWeight={item.weight}
              deleteItemHandler={() => deleteItemFromListHandler(item.id)}
              editItemHandler={() => editItemFromListHandler(item)}
            />
          )}
        />
      </View>
    </TouchableWithoutFeedback>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: COLORS.main,
    flex: 1,
  },
  container: {
    backgroundColor: "#fff",
    borderTopStartRadius: 35,
    borderTopEndRadius: 35,
    flex: 1,
    paddingTop: 5,
  },
  row: {
    flexDirection: "row",
  },
  rowWrapper: {
    width: "65%",
    alignItems: "center",
  },
  rowWrapperCount: {
    width: "35%",
    alignItems: "center",
  },
  field: {
    fontFamily: "MontserratBold",
  },
  rowCounter: {
    flexDirection: "row",
    position: "relative",
  },
  rowMinus: {
    position: "absolute",
    top: 0,
    left: 20,
    zIndex: 1,
  },
  iconText: {
    fontSize: 30,
  },
  rowPlus: {
    position: "absolute",
    top: 0,
    right: 20,
    zIndex: 1,
  },
  buttonWrapper: {
    alignItems: "center",
  },
  buttonEditRow: {
    flexDirection: "row",
  },
  cancelBtn: {
    width: Dimensions.get("screen").width / 2 - 30,
    opacity: 0.5,
    marginRight: 10,
  },
  updateBtn: {
    width: Dimensions.get("screen").width / 2 - 30,
  },
  horizantalLine: {
    height: 3,
    backgroundColor: COLORS.silver,
    marginVertical: 20,
  },
  itemWrapper: {
    paddingHorizontal: 10,
  },
});
