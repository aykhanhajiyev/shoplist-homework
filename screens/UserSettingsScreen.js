import React, { useState, useEffect } from "react";
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Keyboard,
  View,
  Alert,
  Image,
  AsyncStorage
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Permissons from "expo-permissions";

import { CustomText, CustomButton } from "../components";
import { CustomInput } from "../components/CustomInput";

import COLORS from "../styles/colors";
import { connect } from "react-redux";
import { getUser, updateUser,getAllData } from "../store/shops";

const mapStateToProps = (state) => ({
  user: getUser(state),
  allData: getAllData(state),
});

const getPermissons = async () => {
  const { status } = await Permissons.askAsync(Permissons.CAMERA_ROLL);
  if (status !== "granted") {
    Alert.alert("Sorry, you dont have permisson to see camera roll");
    return false;
  }
  return true;
};
export const UserSettingsScreen = connect(mapStateToProps, { updateUser })(
  (props) => {
    const { user, updateUser,allData } = props;
    const [fields, setFields] = useState({
      username: user.username,
      avatarurl: user.image,
    });

    useEffect(() => {
      getPermissons();
      AsyncStorage.setItem("shops", JSON.stringify(allData));
    }, []);

    const changeFieldHandler = (name, value) => {
      setFields((fields) => ({
        ...fields,
        [name]: value,
      }));
    };
    const getImage = async () => {
      try {
        const image = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [1, 1],
        });
        if (!image.cancelled) {
          changeFieldHandler("avatarurl", image.uri);
        }
      } catch (error) {}
    };
    const updateUserHandler = () => {
      updateUser({ username: fields.username, image: fields.avatarurl });
      Alert.alert("Your changes are updated")
    };
    return (
      <TouchableWithoutFeedback
        style={styles.wrapper}
        onPress={Keyboard.dismiss}
      >
        <View style={styles.container}>
          <CustomText style={{ marginBottom: 10, marginTop: 10 }}>
            username
          </CustomText>
          <CustomInput
            value={fields.username}
            onChangeText={(text) => changeFieldHandler("username", text)}
          />
          <CustomText style={{ marginTop: 15 }}>choose avatar</CustomText>
          <Image
            style={styles.userAvatar}
            resizeMode="cover"
            source={{
              uri:
                fields.avatarurl ||
                "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png",
            }}
          />
          <CustomButton
            title="Open Gallery"
            style={{ marginTop: 7 }}
            innerBtnTextWrapperStyle={styles.chooseimage}
            innerTextStyle={{ color: COLORS.main }}
            onPress={getImage}
          />
          <CustomButton title="Save changes" onPress={updateUserHandler} />
        </View>
      </TouchableWithoutFeedback>
    );
  }
);
const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: COLORS.main,
    flex: 1,
  },
  container: {
    alignItems: "center",
    backgroundColor: "#fff",
    borderTopStartRadius: 35,
    borderTopEndRadius: 35,
    flex: 1,
  },
  chooseimage: {
    backgroundColor: COLORS.silver,
  },
  userAvatar: {
    width: 100,
    height: 100,
    backgroundColor: "tomato",
    borderRadius: 50,
  },
});
