import React, { useState, useEffect } from "react";
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Keyboard,
  View,
  Alert,
  AsyncStorage,
} from "react-native";
import {
  CustomText,
  CustomButton,
  CustomOption,
  CustomInput,
} from "../components";

import COLORS from "../styles/colors";
import { connect } from "react-redux";
import { addList, getShops, getAllData } from "../store/shops";

const mapStateToProps = (state) => ({
  shopList: getShops(state),
  allData: getAllData(state),
});

export const AddListScreen = connect(mapStateToProps, { addList })((props) => {
  const { addList, navigation, shopList, allData } = props;
  const [option, setOption] = useState("Regular");
  const [listName, setListName] = useState("");
  const onChange = (option) => {
    setOption(option);
  };
  const resetField = () => {
    setOption("Regular");
    setListName("");
  };
  useEffect(() => {
    AsyncStorage.setItem("shops", JSON.stringify(allData));
  },[]);
  const createListHandler = (listname, type) => {
    if (listname.trim() !== "") {
      addList({ name: listname, type: type });
      navigation.navigate(type === "Regular" ? "Regular" : "OneTime");
      resetField();
    } else {
      Alert.alert("Please fill listname");
    }
  };
  return (
    <TouchableWithoutFeedback style={styles.wrapper} onPress={Keyboard.dismiss}>
      <View style={styles.contentWrapper}>
        <View style={styles.container}>
          <CustomText style={{ marginBottom: 10, marginTop: 10 }}>
            list name
          </CustomText>
          <CustomInput value={listName} onChangeText={setListName} />
          <CustomOption
            options={["One Time", "Regular"]}
            selected={option}
            onChange={onChange}
          />
          <CustomButton
            title="Create List"
            onPress={() => createListHandler(listName, option)}
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  contentWrapper: {
    backgroundColor: COLORS.main,
    flex: 1,
  },
  container: {
    alignItems: "center",
    backgroundColor: "#fff",
    borderTopStartRadius: 35,
    borderTopEndRadius: 35,
    flex: 1,
  },
});
