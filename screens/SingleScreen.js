import React,{useEffect} from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  AsyncStorage,
} from "react-native";
import { connect } from "react-redux";
import { FontAwesome5 } from "@expo/vector-icons";
import { styles } from "./OneTimeListsScreen";
import { SingleListItem, CustomButton, CustomText } from "../components";

import { changeItemBuy, resetBuyItem, getAllData } from "../store/shops";

const mapStateToProps = (state) => ({
  shopList: state.shops.shopList,
  allData: getAllData(state),
});
export const SingleScreen = connect(mapStateToProps, {
  changeItemBuy,
  resetBuyItem,
})((props) => {
  const { navigation, route, changeItemBuy, resetBuyItem, allData } = props;
  const id = route.params.listId;
  const shopItem = props.shopList.find((item) => item.id === id);
  const totalItems = shopItem.items.length;
  const boughtItems = shopItem.items.filter((item) => item.isBought === true)
    .length;
  navigation.setOptions({
    title: route.params.title,
    headerRight: () => (
      <TouchableOpacity
        onPress={() => navigation.navigate("SingleEditScreen", { shopItem })}
      >
        <FontAwesome5 name="pen" size={24} color="white" />
      </TouchableOpacity>
    ),
  });

  useEffect(() => {
    AsyncStorage.setItem("shops", JSON.stringify(allData));
    // console.log("SingleScreen");
  },[]);

  const changeItemBuyHandler = (shopItemID, itemID) => {
    changeItemBuy({ shopItemID, itemID });
  };
  const resetBoughtItem = () => {
    resetBuyItem({ shopItemID: shopItem.id });
  };
  return (
    <View style={styles.container}>
      <View style={stylesSingle.wrapper}>
        <View style={stylesSingle.row}>
          {shopItem.type === "Regular" ? (
            <CustomButton
              title="Reset"
              style={stylesSingle.btn}
              innerTextStyle={stylesSingle.text}
              innerBtnTextWrapperStyle={stylesSingle.btnWrapper}
              onPress={resetBoughtItem}
            />
          ) : (
            <CustomText></CustomText>
          )}
          <CustomText
            style={shopItem.type === "Regular" ? null : { paddingTop: 15 }}
          >
            {boughtItems} / {totalItems}
          </CustomText>
        </View>

        <FlatList
          alwaysBounceVertical={true}
          showsVerticalScrollIndicator={false}
          data={shopItem.items}
          renderItem={({ item }) => (
            <SingleListItem
              itemName={item.name}
              itemCount={item.count}
              itemWeight={item.weight}
              itemIsBought={item.isBought}
              onLongPress={() => changeItemBuyHandler(shopItem.id, item.id)}
            />
          )}
        />
      </View>
    </View>
  );
});

const stylesSingle = StyleSheet.create({
  wrapper: {
    backgroundColor: "#fff",
    flex: 1,
    borderTopStartRadius: 35,
    borderTopEndRadius: 35,
    paddingHorizontal: 25,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  btnWrapper: {
    padding: 5,
    marginBottom: 10,
  },
  btn: {
    width: 72,
  },
  text: {
    fontSize: 12,
  },
});
