import React, { useEffect } from "react";
import { View, StyleSheet, FlatList, StatusBar, Alert,AsyncStorage } from "react-native";
import COLORS from "../styles/colors";

import { connect } from "react-redux";
import { getShops, deleteList, getAllData } from "../store/shops";
import { ListItem } from "../components/ListItem";

const mapStateToProps = (state) => ({
  shopList: getShops(state),
  allData : getAllData(state)
});
export const OneTimeListsScreen = connect(mapStateToProps,{deleteList})((props) => {
  let { navigation, shopList,deleteList, allData } = props;
  shopList = shopList.filter((i) => i.type === "One Time");
  useEffect(() => {
    AsyncStorage.setItem("shops", JSON.stringify(allData));
  },[]);
  const deleteListHandler=(shopItemID)=>{
    Alert.alert("Deleting list","Are you sure to delete it?",[
      {
        text:"No",
        style:'destructive'
      },
      {
        text:"Yes",
        onPress:()=>deleteList({shopItemID})
      }
    ])
  }
  return (
    <>
      <StatusBar
        hidden={false}
        barStyle={"light-content"}
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <FlatList
            alwaysBounceVertical={true}
            showsVerticalScrollIndicator={false}
            data={shopList}
            renderItem={({ item }) => (
              <ListItem
                item={item}
                isRegular={false}
                onPress={() =>
                  navigation.navigate("SingleScreen", {
                    title: item.name,
                    listId: item.id,
                  })
                }
                onLongPress={()=>deleteListHandler(item.id)}
              />
            )}
          />
        </View>
      </View>
    </>
  );
});

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.main,
  },
  wrapper: {
    backgroundColor: "#fff",
    flex: 1,
    borderTopStartRadius: 35,
    borderTopEndRadius: 35,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
});
