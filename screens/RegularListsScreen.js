import React,{useEffect} from "react";
import { View, FlatList, Alert,AsyncStorage } from "react-native";
import { connect } from "react-redux";

import { getShops, deleteList, getAllData } from "../store/shops";
import { styles } from "./OneTimeListsScreen";
import { ListItem } from "../components/ListItem";

const mapStateToProps = (state) => ({
  shopList: getShops(state),
  allData: getAllData(state),
});

export const RegularListsScreen = connect(mapStateToProps, { deleteList })(
  (props) => {
    let { navigation, shopList, deleteList,allData } = props;
    shopList = shopList.filter((i) => i.type === "Regular");
    useEffect(() => {
      AsyncStorage.setItem("shops", JSON.stringify(allData));
    },[]);
    const deleteListHandler = (shopItemID) => {
      Alert.alert("Deleting list", "Are you sure to delete it?", [
        {
          text: "No",
          style: "destructive",
        },
        {
          text: "Yes",
          onPress: () => deleteList({ shopItemID }),
        },
      ]);
    };
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <FlatList
            alwaysBounceVertical={true}
            showsVerticalScrollIndicator={false}
            data={shopList}
            renderItem={({ item }) => (
              <ListItem
                item={item}
                isRegular={true}
                onPress={() =>
                  navigation.navigate("SingleScreen", {
                    title: item.name,
                    listId: item.id,
                  })
                }
                onLongPress={() => deleteListHandler(item.id)}
              />
            )}
          />
        </View>
      </View>
    );
  }
);
