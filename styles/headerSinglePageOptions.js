import React from "react";
import { TouchableOpacity } from "react-native";

import { Ionicons } from "@expo/vector-icons";
const headerSinglePageOptions = {
  headerLeft: (props) => (
    <TouchableOpacity onPress={() => props.onPress()}>
      <Ionicons name="ios-arrow-round-back" size={32} color="white" />
    </TouchableOpacity>
  ),
  headerLeftContainerStyle: {
    marginLeft: 10,
  },
};
export default headerSinglePageOptions;