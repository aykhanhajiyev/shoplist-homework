import COLORS from "../styles/colors";
const headerStyleOptions = {
  headerTitleStyle: { fontFamily: "MontserratSemiBold" },
  headerStyle: { backgroundColor: COLORS.main },
  headerTintColor: "#fff",
  headerTitleAlign: "center",
  headerRightContainerStyle: { marginRight: 20 },
};

export default headerStyleOptions;
