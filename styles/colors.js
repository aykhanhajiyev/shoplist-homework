const COLORS = {
    main:"#FF7676",
    light:"#FFE194",
    secondary:"#FFD976",
    dark:"#303234",
    silver:"#EEEEEE"
}

export default COLORS;