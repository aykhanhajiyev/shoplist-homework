import React, {Component} from "react";
import { StyleSheet, AsyncStorage } from "react-native";
import { AppLoading } from "expo";
import { loadFonts } from "./styles/fonts";
import { RootNav } from "./navigation";
import { Provider } from "react-redux";
import store from "./store";

export default class App extends Component {
  state = {
    loaded: false,
  };
  async componentDidMount (){
    const data = await AsyncStorage.getItem("shops");    
    const jsonData = JSON.parse(data);
    console.log(jsonData)
    if(jsonData!==null){
      store.getState().shops = {...jsonData};
    }
    console.log(store.getState().shops);
  }
  render() {
    if (!this.state.loaded) {
      return (
        <AppLoading
          startAsync={loadFonts}
          onFinish={() => this.setState({ loaded: true })}
          onError={() => console.log("Loading Rejected")}
        />
      );
    }
    return (
      <Provider store={store}>
        <RootNav />
      </Provider>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
